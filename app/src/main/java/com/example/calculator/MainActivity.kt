package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var firstVariable = 0.0
    private var secondVariable = 0.0
    private var operation =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }
    private fun init(){
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
        dotButton.setOnClickListener(){
            val value =resultTextView.text.toString()
            if (value.isNotEmpty() && "." !in value)
                resultTextView.text = resultTextView.text.toString() + "."

        }
        deleteButton.setOnLongClickListener(){
            resultTextView.text=""
            return@setOnLongClickListener true
        }


    }
    fun delete(view:View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty())
            resultTextView.text = value.substring(0, value.length - 1)

    }

    fun divide(view: View){
        val value =resultTextView.text.toString()

        if (value.isNotEmpty()) {
            operation = ":"
            firstVariable = value.toDouble()
            resultTextView.text = ""
        }


    }
    fun equal(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty() && operation!="") {
            secondVariable = value.toDouble()
            var result: Double = 0.0
            if (operation == ":" && secondVariable!=0.0) {
                result = firstVariable / secondVariable
                resultTextView.text = result.toString()
            }
            else if (operation == "+") {
                result = firstVariable + secondVariable
                resultTextView.text = result.toString()
            }
            else if (operation == "-") {
                result = firstVariable - secondVariable
                resultTextView.text = result.toString()
            }
            else if (operation == "x") {
                result = firstVariable * secondVariable
                resultTextView.text = result.toString()
            }
            else if(operation ==":" && secondVariable ==0.0){
                resultTextView.text="zeroDivision"
                Toast.makeText(this, "can't be divided by 0", Toast.LENGTH_SHORT).show()
            }



        }
    }
    fun multiply(view: View){
        val value = resultTextView.text.toString()

        if(value.isNotEmpty()){
            operation = "x"
            firstVariable = value.toDouble()
            resultTextView.text = ""

        }

    }
    fun plus(view: View){
        val value =resultTextView.text.toString()

        if (value.isNotEmpty()) {
            operation = "+"
            firstVariable = value.toDouble()
            resultTextView.text = ""
        }

    }
    fun minus(view: View){
        val value =resultTextView.text.toString()

        if(value.isNotEmpty()){
            operation = "-"
            firstVariable = value.toDouble()
            resultTextView.text = ""
        }

    }

    override fun onClick(v: View?) {
        val button = v as Button
        resultTextView.text = resultTextView.text.toString() + button.text.toString()

    }

}